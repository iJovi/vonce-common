package cn.vonce.common.bean;

/**
 * 用于返回分页数据结果集(Result Set)
 *
 * @author jovi
 * @version 1.0
 * @email 766255988@qq.com
 * @date 2017年5月24日上午11:32:38
 */
public class PagingRS extends RS {


    public static final String PAGE_NUM = "pagenum";
    public static final String PAGE_SIZE = "pagesize";
    public static final String TOTAL_RECORDS = "totalRecords";
    public static final String TOTAL_PAGE = "totalPage";
    public static final String TIMESTAMP = "timestamp";

    public Integer getPagenum() {
        Object value = super.get(PagingRS.PAGE_NUM);
        if (value == null) {
            return null;
        }
        return Integer.parseInt(value.toString());
    }

    public void setPagenum(Integer pagenum) {
        super.put(PagingRS.PAGE_NUM, pagenum);
    }

    public Integer getPagesize() {
        Object value = super.get(PagingRS.PAGE_SIZE);
        if (value == null) {
            return null;
        }
        return Integer.parseInt(value.toString());
    }

    public void setPagesize(Integer pagesize) {
        super.put(PagingRS.PAGE_SIZE, pagesize);
    }

    public Integer getTotalRecords() {
        Object value = super.get(PagingRS.TOTAL_RECORDS);
        if (value == null) {
            return null;
        }
        return Integer.parseInt(value.toString());
    }

    public void setTotalRecords(Integer totalRecords) {
        super.put(PagingRS.TOTAL_RECORDS, totalRecords);
    }

    public Integer getTotalPage() {
        Object value = super.get(PagingRS.TOTAL_PAGE);
        if (value == null) {
            return null;
        }
        return Integer.parseInt(value.toString());
    }

    public void setTotalPage(Integer totalPage) {
        super.put(PagingRS.TOTAL_PAGE, totalPage);
    }

    public String getTimestamp() {
        Object value = super.get(PagingRS.TIMESTAMP);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    public void setTimestamp(String timestamp) {
        super.put(PagingRS.TIMESTAMP, timestamp);
    }

}
